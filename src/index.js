import Vue from 'vue'
import App from './components/App.vue'
import AsyncComputed from 'vue-async-computed'

Vue.use(AsyncComputed)
Vue.config.productionTip = false

new Vue({
  el: '#app',
  render: h => h(App)
})
