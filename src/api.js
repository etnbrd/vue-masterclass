import { key } from '../key'
import axios from 'axios'

export async function search(query) {
  const { data } = await axios.get(`https://api.themoviedb.org/3/search/tv?query=${query}&api_key=${key}`)

  return data.results
    .filter(result => result.poster_path)
    .map(result => ({
      id: result.id,
      title: result.name,
      poster: `https://image.tmdb.org/t/p/w500${result.poster_path}`,
      rating: Number(result.vote_average),
      first_air_date: new Date(result.first_air_date)
    }))
}
